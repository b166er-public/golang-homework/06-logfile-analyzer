package main

/**
* !!! YOU ARE NOT ALLOWED TO CHANGE ANY LINE IN THIS FILE !!!
 */

import (
	"fmt"
	"os"
	"private/logfile_analyzer/logic"
)

func run_test_1() bool {
	// Load logfile
	logfile := logic.LoadChatLogFile(
		os.Args[1] + "run_1.log")

	// Check Dima
	if logfile.CountSessionsOfUser("Dima") != 28 {
		return false
	}

	if logfile.CountSessionTimeByUser("Dima") != 59468 {
		return false
	}

	// Check @DoNotSpeakWithMe
	if logfile.CountSessionsOfUser("@DoNotSpeakWithMe") != 23 {
		return false
	}

	if logfile.CountSessionTimeByUser("@DoNotSpeakWithMe") != 49839 {
		return false
	}

	return true

}

func run_test_2() bool {
	// Load logfile
	logfile := logic.LoadChatLogFile(
		os.Args[1] + "run_2.log")

	// Check ThePope
	if logfile.CountSessionsOfUser("ThePope") != 25 {
		return false
	}

	if logfile.CountSessionTimeByUser("ThePope") != 41115 {
		return false
	}

	// Check Marina
	if logfile.CountSessionsOfUser("Marina") != 21 {
		return false
	}

	if logfile.CountSessionTimeByUser("Marina") != 36096 {
		return false
	}

	return true
}

func run_test_3() bool {
	// Load logfile
	logfile := logic.LoadChatLogFile(
		os.Args[1] + "run_3.log")

	// Check Best User Ever
	if logfile.CountSessionsOfUser("Best User Ever") != 23 {
		return false
	}

	if logfile.CountSessionTimeByUser("Best User Ever") != 41352 {
		return false
	}

	// Check Marina
	if logfile.CountSessionsOfUser("Paul") != 26 {
		return false
	}

	if logfile.CountSessionTimeByUser("Paul") != 48380 {
		return false
	}

	return true
}

func main() {

	if run_test_1() {
		fmt.Println("Test 1 ... OK!")
	} else {
		fmt.Println("Test 1 ... FAILED!")
	}

	if run_test_2() {
		fmt.Println("Test 2 ... OK!")
	} else {
		fmt.Println("Test 2 ... FAILED!")
	}

	if run_test_3() {
		fmt.Println("Test 3 ... OK!")
	} else {
		fmt.Println("Test 3 ... FAILED!")
	}

}

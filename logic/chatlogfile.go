package logic

import (
	"os"
	"regexp"
	"strconv"
	"strings"
)

type ChatLogFile struct {
	enters_to_chat  map[string][]uint
	exits_from_chat map[string][]uint
}

var re = regexp.MustCompile(`^Time\s:\s(\d+)\sUser\s\[(.+)\]\s(.+)\schat\!$`)

func (this *ChatLogFile) CountSessionsOfUser(user_name string) uint {
	return uint(len(this.enters_to_chat[user_name]))
}

func (this *ChatLogFile) CountSessionTimeByUser(user_name string) uint {
	totalSessionTime := uint(0)

	enterTimes := this.enters_to_chat[user_name]
	exitTimes := this.exits_from_chat[user_name]

	for i := 0; i < len(exitTimes); i++ {
		totalSessionTime += exitTimes[i] - enterTimes[i]
	}

	return totalSessionTime
}

func LoadChatLogFile(logfile_path string) *ChatLogFile {
	chatLogFile := &ChatLogFile{
		enters_to_chat:  make(map[string][]uint),
		exits_from_chat: make(map[string][]uint),
	}

	text, err := os.ReadFile(logfile_path)
	if err != nil {
		panic(err)
	}

	str := string(text)

	for _, line := range strings.Split(str, "\n") {
		match := re.FindStringSubmatch(line)
		if match != nil {
			unix := match[1]
			user := match[2]
			action := match[3]

			time, err := strconv.Atoi(unix)
			if err != nil {
				continue
			}

			if action == "entered" {
				chatLogFile.enters_to_chat[user] = append(chatLogFile.enters_to_chat[user], uint(time))
			} else if action == "left" {
				chatLogFile.exits_from_chat[user] = append(chatLogFile.exits_from_chat[user], uint(time))
			}
		}

	}

	return chatLogFile
}
